from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
import logging
import time
import os
import json
from datetime import datetime
from bs4 import BeautifulSoup

userAgent = "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/96.0.4664.110 Safari/537.36"

try:  
    options = Options()
    options.headless = True
    options.add_argument(f'user-agent={userAgent}')
    options.add_argument("window-size=1280,800")
    options.add_experimental_option("prefs", { "profile.default_content_setting_values.notifications": 1})
    # For older ChromeDriver under version 79.0.3945.16
    options.add_experimental_option("excludeSwitches", ["enable-automation"])
    options.add_experimental_option('useAutomationExtension', False)
    #For ChromeDriver version 79.0.3945.16 or over
    options.add_argument('--disable-blink-features=AutomationControlled')
    driver = webdriver.Chrome(chrome_options=options)
    driver.get("https://www.back4app.com/database/back4app/usa-by-state")
    time.sleep(5)
    soup = BeautifulSoup(driver.page_source, 'html.parser')
    driver.close()
    table = soup.find('table')
    rows = table.find_all('tr')
    columns = table.find_all('td')
    state_dict = {}
    for i in columns:
        try:
            text = i.get_text()
            div = i.find('div')
            title = div.get('title')
            href = div.find('a').get('href')
            state_dict[title] = {'title': title, 'href': href}
        except:
            pass

    for i in state_dict.items():
        href = i[1]['href']
        shortcode = i[1]['title']
        url = f'https://www.back4app.com{href}'
        print("url: ", url)
        driver = webdriver.Chrome(chrome_options=options)
        driver.get(url)
        time.sleep(2)
        driver.execute_script("window.scrollTo(0, document.body.scrollHeight);")
        last = driver.find_element_by_xpath("//button[contains(text(), 'Last')]")
        last.click()
        soup = BeautifulSoup(driver.page_source, 'html.parser')
        li_main = soup.find_all('li', class_='page-item')
        pages_check = []
        for i in li_main:
            try:
                pages_check.append(i.get_text())
            except:
                pass
        pages_check = [i for i in pages_check if i not in ['First', 'Previous', 'Next', 'Last']]
        last_page = pages_check[-1]            
        page_amount = int(last_page)
        print(pages_check)
        print("page_amount: ", page_amount)
        driver.close()

        driver = webdriver.Chrome(chrome_options=options)
        driver.get(url)
        time.sleep(2)
        button = driver.find_element_by_xpath("//button[contains(text(), 'Next')]")
        driver.execute_script("window.scrollTo(0, document.body.scrollHeight);")

        cities = []
        for i in range(1, page_amount+1):
            driver.execute_script("window.scrollTo(0, document.body.scrollHeight);")
            soup = BeautifulSoup(driver.page_source, 'html.parser')
            table = soup.find('table')
            tbody = table.find('tbody')
            
            thead = table.find('thead')
            tr = tbody.find_all('tr')[1] 
            for i, th in enumerate(thead.find_all('th')):
                if th.text == 'name':
                    idx = i
            print("name is at index: ", idx)
            titles = tbody.find_all('tr')
            for i in titles:
                try:
                    city = i.find_all('td')[idx]
                    div = city.find('div')
                    title = div.get('title')
                    cities.append(title)
                    print("title: ", title)
                except Exception as e:
                    print("error: ", e)
                    pass
            button.click()
        driver.close()
        to_json = {shortcode: cities}
        cities_json = json.dumps(to_json)
        with open(f'/home/gary/cities/{shortcode}-cities.json', 'w') as f:
            f.write(cities_json)
        print(f'{shortcode} cities saved')
except Exception as e:
    print(e)
