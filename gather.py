import requests
import json
import sys
import os
import csv
import urllib
import time
import argparse
from bs4 import BeautifulSoup

states = ["AL","AK","AZ","AR","CA","CO","CT","DE","FL","GA","HI","ID","IL","IN","IA","KS","KY","LA","ME","MD","MA","MI","MN","MS","MO","MT","NE","NV","NH","NJ","NM","NY","NC","ND","OH","OK","OR","PA","RI","SC","SD","TN","TX","UT","VT","VA","WA","WV","WI","WY"]
row_default_head = ['MemberId', 'PersonId', 'MiddleName', 'PreferredPronoun', 'Title', 'Gender', 'MemberTypeCode', 'MemberTypeCodeDescription', 'MemberStatusCode', 'MemberStatusCodeDescription', 'MemberStatusDate', 'JoinedDate', 'PrimaryLocalAssociationId','LocalAssociationTypeCode', 'LocalAssociationName', 'LocalAssociationState', 'StateAssociationTypeCode', 'StateAssociationName', 'PrimaryStateAssociationId', 'PrimaryOfficeId', 'PrimaryReLicenseNumber', 'PrimaryReLicenseState', 'MemberPrimaryInd', 'MemberSubClass', 'ArbitrationEthicsPending', 'DesignatedRealtorFlag', 'LocalJoinDate', 'SpecialDiscountFlag', 'DuesWaivedLocalFlag', 'DuesWaivedNationalFlag', 'DuesWaivedStateFlag', 'Generation', 'NickName', 'BirthDate', 'DirectMarketingFlag', 'OccupationName', 'OfficeVoiceMailExtensionNumber', 'SingleOwnedMlsStatusCode', 'SingleOwnedMlsStatusDate', 'OrientationDate', 'PreviousNonMemberFlag', 'ReinstatementCode', 'ReinstatementDate', 'StopPublicationFlag', 'PreferredMailTypeCode', 'PreferredPublicationTypeCode', 'PreferredPhoneTypeCode', 'PreferredFaxTypeCode', 'StopFaxFlag', 'StopEmailFlag', 'WebPageAddress', 'MLS', 'OnRosterFlag', 'StopMailFlag', 'IsPoeFlag', 'IsPoeProxyFlag', 'PrimaryFOBId', 'PrimaryFieldOfBusinessId', 'PrimaryFieldOfBusinessName', 'SecondaryFOBId1', 'SecondaryFieldOfBusinessId1', 'SecondaryFieldOfBusinessName1', 'SecondaryFOBId2', 'SecondaryFieldOfBusinessId2', 'SecondaryFieldOfBusinessName2', 'SecondaryFOBId3', 'SecondaryFieldOfBusinessId3', 'SecondaryFieldOfBusinessName3', 'TeamName', 'TeamId', 'MemberSecondaries', 'MemberSecondariesCount', 'MemberCertification', 'MemberCertificationCount', 'MemberCoe', 'MemberCoeCount', 'MemberDemographic', 'MemberDemographicCount', 'MemberDesignation', 'MemberDesignationCount', 'MemberDuesPayment', 'MemberDuesPaymentCount', 'MemberEducationLevel', 'MemberEducationLevelCount', 'MemberEducation', 'MemberEducationCount', 'MemberIscAffiliation', 'MemberIscAffiliationCount', 'MemberLanguage', 'MemberLanguageCount', 'MemberMilitary', 'MemberMilitaryCount', 'MemberMLS', 'MemberMLSCount', 'MemberSingleOwnedMLS', 'MemberSingleOwnedMLSCount', 'HomeAddressId', 'HomeAddressTypeCode', 'HomeAddressLine1', 'HomeAddressLine2', 'HomeAddressCity', 'HomeAddressState', 'HomeAddressZip', 'HomeAddressZip6', 'MailAddressId', 'MailAddressTypeCode', 'MailAddressLine1', 'MailAddressLine2', 'MailAddressCity', 'MailAddressState','MailAddressZip', 'MailAddressZip6', 'PersonalEmailAddressId', 'PersonalEmailAddress', 'BusinessEmailAddressId', 'BusinessEmailAddress', 'SharedEmailAddressId', 'SharedEmailAddress', 'TeamEmailAddressId', 'TeamEmailAddress', 'HomePhoneNumberId', 'HomePhoneNumber', 'DirectDialNumberId', 'DirectDialNumber', 'CellPhoneNumberId', 'CellPhoneNumber', 'PersonalFaxNumberId', 'PersonalFaxNumber', 'MemberMasterRecord', 'OfficeId', 'OfficeBusinessName', 'BillingOfficeId', 'BranchType', 'PrimaryLocalAssociationId', 'PrimaryStateAssociationId', 'OfficeStatusCode', 'OfficeStatusDate', 'CorporateLicenseNumber', 'OfficeEmailAddress', 'FranchiseOfficeId', 'JoinedDate', 'StreetAddressLine1', 'StreetAddressLine2', 'StreetCity', 'StreetState', 'StreetZip', 'StreetZip6', 'MainOfficeId', 'RegionalMlsId', 'MlsOnlineStatus', 'MlsOnlineStatusDate', 'NmSalespersonCount', 'DirectMarketingMailFlag', 'MailingAddressLine1', 'MailingAddressLine2', 'MailingCity', 'MailingState', 'MailingZip', 'MailingZip6', 'OfficeContactDrId', 'OfficeContactDrName', 'OfficeContactManagerId', 'OfficeContactManagerName', 'OfficeContactUnlicensed', 'OfficeCorporateName', 'OfficeFaxNumber', 'OfficePhoneNumber', 'OfficeStopFaxFlag', 'OfficeTypeDescription', 'OfficePrimaryIndicator', 'ParentCompanyOfficeId', 'OfficeStopMailFlag', 'TaxIdNumber', 'WebPageAddress', 'TransferDate', 'AdditionalPhoneNumber', 'OfficeFormalName', 'TimeStampSeq', 'BillingOffice', 'MainOffice', 'FranchiseOffice', 'ParentCompanyOffice', 'RefOfficeStatusType', 'RefBranchType', 'OfficeContactDr', 'OfficeContactManager', 'PrimaryLocalAssociation', 'PrimaryStateAssociation', 'RefStreetState', 'RefMailState', 'OfficeSecondaries', 'FirstInsertedBy', 'FirstInsertedDateTime', 'LastChangedBy', 'LastChangedDateTime', 'SourceSystemId', 'SourceMethodId', 'CoreEntityStatusCode', 'CanEdit', 'SourceSystemDisplayName', 'SourceSystemDisplayName', 'TimeStampSeq', 'SearchResultsName', 'FirstInsertedBy', 'FirstInsertedDateTime', 'LastChangedBy', 'LastChangedDateTime', 'SourceSystemId', 'SourceMethodId', 'FirstName', 'LastName', 'CoreEntityStatusCode', 'CanEdit', 'SourceSystemDisplayName']
row_agent_head = ['PersonId', 'MiddleName', 'MemberTypeCode', 'MemberTypeCodeDescription', 'PrimaryLocalAssociationId', 'LocalAssociationName', 'PrimaryStateAssociationId', 'StateAssociationName', 'PrimaryOfficeId', 'Generation', 'WebPageAddress', 'BusinessEmailAddress', 'PrimaryFieldOfBusinessId', 'PrimaryFieldOfBusinessName', 'SecondaryFieldOfBusinessId1', 'SecondaryFieldOfBusinessName1', 'SecondaryFieldOfBusinessId2', 'SecondaryFieldOfBusinessName2', 'SecondaryFieldOfBusinessId3', 'SecondaryFieldOfBusinessName3', 'MemberCertification', 'MemberLanguage', 'MemberDesignation', 'Office', 'FirstInsertedBy', 'FirstInsertedDateTime', 'LastChangedBy', 'LastChangedDateTime', 'FirstName', 'LastName', 'SourceSystemDisplayName']
basic_auth = 'Basic bmFycmVhbHRvcmRpcmVjdG9yeTokV2Q/S0huN15Va3EtcWo1'

parser = argparse.ArgumentParser(description='GATHER REALESTATE DATA')
parser.add_argument('-t', '--throttle', help='Throttle sequence. Example: python3 gather.py -t 5 -r run', required=False)
parser.add_argument('-i', '--input', help='Input data. Change data inside input.json', required=False)
parser.add_argument('-r', '--run', help='Execute program. Example: python3 gather.py -r run', required=False)
parser.add_argument('-d', '--delete', help='Delete all data. Example: python3 gather.py -d purge', required=False)

def null_check(data):
    if data is None:
        return "NONE"
    else:
        return data

def get_state_name_from_shortcode(state_code):
    cities = []
    with open(f'cities/{state_code}-cities.json') as json_file:
        data = json.load(json_file)    
    return data[f'{state_code}']

def header_default_ep():
    global basic_auth
    return {
        'authority': 'nar.m1gateway.realtor',
        'accept': 'application/json, text/plain, */*',
        'accept-language': 'en-GB,en-US;q=0.9,en;q=0.8',
        'authorization': basic_auth,
        'content-type': 'application/json',
        'origin': 'https://directories.apps.realtor',
        'referer': 'https://directories.apps.realtor/',
        'sec-ch-ua': '.Not/A)Brand";v="99", "Google Chrome";v="103", "Chromium";v="103"',
        'sec-ch-ua-mobile': '?0',
        'sec-ch-ua-platform': 'Linux',
        'sec-fetch-dest': 'empty',
        'sec-fetch-mode': 'cors',
        'sec-fetch-site': 'cross-site',
        'user-agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/103.0.0.0 Safari/537.36',
    }

def payload_default_ep(state, city, letter):
    return {
        'officeStreetCountry': 'US',
        'officeStreetState': f'{state}',
        'officeStreetCity': f'{city}',
        'memberFirstName': "{}".format(letter),
    }

def header_agent_details():
    global basic_auth
    return {
        'authority': 'nar.m1gateway.realtor',
        'accept': 'application/json, text/plain, */*',
        'accept-language': 'en-GB,en-US;q=0.9,en;q=0.8',
        'authorization': basic_auth,
        'content-type': 'application/json',
        'origin': 'https://directories.apps.realtor',
        'referer': 'https://directories.apps.realtor/',
        'sec-ch-ua': '.Not/A)Brand";v="99", "Google Chrome";v="103", "Chromium";v="103"',
        'sec-ch-ua-mobile': '?0',
        'sec-ch-ua-platform': 'Linux',
        'sec-fetch-dest': 'empty',
        'sec-fetch-mode': 'cors',
        'sec-fetch-site': 'cross-site',
        'user-agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/103.0.0.0 Safari/537.36',
    }

def payload_agent_details(id, country, lastname):
    return {
        "personId": f'{id}',
        "officeStreetCountry": f'{country}',
        "memberLastName": f'{lastname.upper()}'
        }

def remove_file(file):
    os.remove(file)

def write_custom_data():
    global row_agent_head
    with open('input.json', 'r') as outfile:
        json_data = json.load(outfile)
    STREETCITY = json_data['Office']['StreetCity']
    STREETSTATE = json_data['Office']['StreetState']
    STREETCITY = STREETCITY.lower()
    STREETCITY = STREETCITY[0].upper() + STREETCITY[1:]
    row = []
    for key, value in json_data.items():
        if isinstance(value, dict):
            for key2, value2 in value.items():
                row.append(value2)
        else:
            row.append(value)
    file_name = f'extracted/AGENT-{STREETSTATE}-{STREETCITY}.csv'
    if os.path.isfile(file_name):
        print("File detected. Inserting into", file_name)
        with open(file_name, 'a') as csvfile:
            writer = csv.writer(csvfile)
            writer.writerow(row)
    else:
        print("File not detected. Creating", file_name)
        with open(file_name, 'w') as csvfile:
            writer = csv.writer(csvfile)
            writer.writerow(row_agent_head)
            writer.writerow(row)

        
if __name__ == "__main__":
    args = parser.parse_args()
    throttle = 0
    if args.throttle:
        try:
            throttle = int(args.throttle)
            print(f'[!] Throttle set to {throttle}')
        except ValueError:
            print('[!] Throttle must be a number')
            exit()
    if args.run:
        if "run" in args.run:
            print("[!] Starting...")
            print("*If this is your first time using this program, please read the README.md file.*")
            for letter in range(ord('a'), ord('z') + 1):
                time.sleep(throttle)
                print("[+] Letter: {}".format(chr(letter)))
                for state in states:
                    file_default = f'extracted/STATE-{state}.csv'
                    if os.path.isfile(file_default):
                        writer_default = csv.writer(open(file_default, 'a'))
                    else:
                        writer_default = csv.writer(open(file_default, 'w'))
                    cities = get_state_name_from_shortcode(state)
                    print("[+] State: {}".format(state))
                    writer_default.writerow(row_default_head)
                    for city in cities:
                        file_agent = f'extracted/AGENT-{state}-{city}.csv'
                        if os.path.isfile(file_agent):
                            writer_agent = csv.writer(open(file_agent, 'a'))
                        else:
                            writer_agent = csv.writer(open(file_agent, 'w'))
                        writer_agent.writerow(row_agent_head)
                        print("------------------------------------------------------")
                        print("[+] City: {}".format(city))
                        print("[+] Letter: {}".format(chr(letter)))
                        print("[+] State: {}".format(state))
                        url = 'https://nar.m1gateway.realtor/ext/Member/Search/Directory'
                        payload = payload_default_ep(state, city, chr(letter))
                        headers = header_default_ep()
                        print("-----------------------------------------------------")
                        response = requests.post(url, headers=headers, data=json.dumps(payload))
                        json_data = json.loads(response.text)
                        if json_data:
                            for i in json_data:
                                data = dict(i)
                                row = []
                                lastname = str(data['LastName'])
                                personId = str(data['PersonId'])
                                print("[+] Lastname: {}".format(lastname))
                                print("[+] Personid: {}".format(personId))
                                url_agent_details = 'https://nar.m1gateway.realtor/ext/Member/Directory'
                                payload_agent_detail = payload_agent_details(personId, 'US', lastname)
                                headers_agent_detail = header_agent_details()
                                response_agent_details = requests.post(url_agent_details, headers=headers_agent_detail, data=json.dumps(payload_agent_detail))
                                json_data_agent_details = json.loads(response_agent_details.text)
                                if json_data_agent_details:
                                    row_agent = []
                                    for i, k in json_data_agent_details.items():
                                        row_agent.append(str(k))

                                    writer_agent.writerow(row_agent)
                    
                                for key, value in data.items():
                    
                                    if isinstance(value, dict):
                                        office = dict(value)
                                        for key, value in office.items():
                                            row.append(null_check(value))

                                    row.append(null_check(value))
                                writer_default.writerow(row)
                        print("------------------------------------------------------")
                        print("[-] No data found for {} {}".format(state, city))
                        print("------------------------------------------------------")
        else:
            print("[!] Argument error detected")
            print("[!] Please use --run run")
            exit()

    if args.input:
        if "insert" in args.input:
            write_custom_data()
            exit()
        else:
            print("[!] Invalid input")
            print("[!] Please use --input insert")
            exit()

    if args.delete:
        if "purge" in args.delete:
            for file in os.listdir('extracted'):
                remove_file(f'extracted/{file}')
            print("[!] Files deleted")
            exit()
        else:
            print("[!] Invalid input")
            print("[!] Please use --delete purge")
            exit()

    if not args.run:
        print("[!] No RUN arguments detected. Exiting...")
        print("[!] Please launch the program with the -r flag to run the script")
                
                